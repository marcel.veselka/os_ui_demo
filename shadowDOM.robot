# # # # # # # # # # # # # # # # # # # # # # # # # #
#  Print dialog related KWs
#  Controls dialogs via Shadow DOM: https://developers.google.com/web/fundamentals/web-components/shadowdom?hl=en
# # # # # # # # # # # # # # # # # # # # # # # # # #

*** Keywords ***
Wait Until Print Dialog Open
    Wait Until Keyword Succeeds  5s  1s  Print Dialog Is Open
    ${hnls}=  Get Window Handles
    Switch Window  ${hnls}[1]  timeout=5s

Print Dialog Is Open
    ${w handles}  Get Window Handles
    ${n handlers}  Get Length  ${w handles}
    Should Be Equal As Integers  ${n handlers}  2

Click Cancel Button
    Execute Javascript  return document.querySelector("print-preview-app")
    ...  .shadowRoot.querySelector("print-preview-sidebar")
    ...  .shadowRoot.querySelector("print-preview-button-strip")
    ...  .shadowRoot.querySelector("cr-button.cancel-button").click()

Click Print Button
    Execute Javascript  return document.querySelector("print-preview-app")
    ...  .shadowRoot.querySelector("print-preview-sidebar")
    ...  .shadowRoot.querySelector("print-preview-button-strip")
    ...  .shadowRoot.querySelector("cr-button.action-button").click()

Select Print to PDF
    Execute Javascript  return document.querySelector("print-preview-app")
    ...  .shadowRoot.querySelector("print-preview-sidebar")
    ...  .shadowRoot.querySelector("print-preview-button-strip")
    ...  .shadowRoot.querySelector("cr-button.cancel-button").click()
