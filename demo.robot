*** Settings ***
Library  SeleniumLibrary
Library  String
Library  send_keys.py

Resource  shadowDOM.robot

Test Teardown   Close All Browsers

*** Variables ***
${BROWSER}          Chrome  # headlesschrome  # Chrome
${LOGIN NAME}       %{LOGIN_NAME}
${PASSWD}           %{PASSWD}
${LOGIN STRING}     ${LOGIN_NAME}:${PASSWD}@
${HOST}             False
${URL}              http://testlab.tesena.com/testapp
${COMPOUND URL}     http://${LOGIN STRING}testlab.tesena.com/testapp
${UPLOAD URL}       http://${LOGIN STRING}testlab.tesena.com/demo/upload.html
${URL2}             https://testguild.com/SeleniumTestPage.html
${UPLOAD PATH}      ${CURDIR}


*** Test Cases ***
TC1 - Login With Compound URL
    [Documentation]  Works locally on dev machine as well as in Selenium Docker container
    Open Browser  ${COMPOUND URL}  ${BROWSER}  remote_url=${HOST}
    Assertions After Login

TC2 - Login With Normal URL
    [Documentation]  Works locally on dev machine but NOT in Selenium Docker container (xvfb installation would fix it)
    Open Browser  ${URL}  ${BROWSER}  remote_url=${HOST}
    Send Keys Login  ${LOGIN NAME}  ${PASSWD}
    Assertions After Login

TC3 - Print Dialog: Shadow DOM example
    Open Browser  ${URL2}  ${BROWSER}  remote_url=${HOST}
#    ...  options=add_argument('--no-sandbox'); add_argument("--disable-gpu"); add_argument("--print-to-pdf=${CURDIR}"); add_argument("--disable-dev-shm-usage")
    Click Button  printButton
    Capture Page Screenshot
    Wait Until Print Dialog Open
    Capture Page Screenshot
#    Click Print Button
    Click Cancel Button

TC4 - Upload File Dialog
    Open Browser  ${UPLOAD URL}  ${BROWSER}  remote_url=${HOST}
    # https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html#Choose%20File
    Choose File	fileToUpload  ${UPLOAD PATH}\\upload_test.txt

#TC6 - Example: Ignore certificates
#    Open Browser  ${URL}  ${BROWSER}  remote_url=${HOST}  options=add_argument("--ignore-certificate-errors")
#    Wait Until Page Contains  Hello World!

*** Keywords ***
Assert Upload Form Is Visibile
    Wait Until Element Is Visible       name:fileToUpload

Assertions After Login  # just for example, might be changed
    Wait Until Page Contains            Welcome to Smart Banking
    Wait Until Page Contains Element    //div[contains(@class, 'well')]