import time
import pyautogui

def send_keys_login(name, password):
    '''
    Wokrs on Windows/Linux, settings name, pass and press enter
    '''
    pyautogui.write(name)
    pyautogui.press('tab')
    pyautogui.write(password)
    pyautogui.press('enter')

def send_keys_print():
    '''
    Wokrs on Windows/Linux just press ctrl+p and press enter
    '''
    pyautogui.hotkey('ctrl', 'p')
    time.sleep(2)
    pyautogui.press('enter')

def send_item(path):
    '''
    Wokrs on Windows/Linux just write path to file and press enter
    '''
    time.sleep(2)
    pyautogui.write(path)
    pyautogui.press('enter')
